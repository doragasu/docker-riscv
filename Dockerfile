FROM debian:bullseye
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update -y && apt-get install -y build-essential libnewlib-dev gcc-riscv64-unknown-elf libusb-1.0-0-dev libudev-dev gdb-multiarch
